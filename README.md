#Java string counter based on netty

##Proto
First of all you need to generate protobuf-classes by running following command in source directory:
```
mvn compile
```
Files will be generated automatically by schemas defined in <em>"/esjswbarysole/proto"</em> folder.

<sub><sup><em>** to avoid any possible errors in IDE update maven project</em><sup><sub>

##Running
When all of generated classes was created, you can finally run this java application.