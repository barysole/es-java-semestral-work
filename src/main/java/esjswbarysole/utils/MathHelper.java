package esjswbarysole.utils;

public class MathHelper {
    public static int smallestGreaterPowerOfTwo(int minSize) {
        return 1 << (32 - Integer.numberOfLeadingZeros(minSize - 1));
    }
}
