package esjswbarysole;

import esjswbarysole.utils.MathHelper;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

public class NonblockStringSet {

    private final int mask;
    private final AtomicInteger size = new AtomicInteger();
    private final int binsLength;
    private AtomicReferenceArray<Node> bins;

    public NonblockStringSet(int minSize) {
        if (minSize <= 0) {
            throw new IllegalArgumentException("Size must be greater than 0");
        }
        this.binsLength = MathHelper.smallestGreaterPowerOfTwo(minSize);
        this.mask = binsLength - 1;
        this.bins = new AtomicReferenceArray<>(new Node[binsLength]);
    }

    public void resetSet() {
        this.bins = new AtomicReferenceArray<>(new Node[binsLength]);
    }

    public void add(String word) {
        int binIndex = getBinIndex(word);
        //try to read and write in arr
        Node newBin = new Node(word, null);
        while (true) {
            Node bin = bins.get(binIndex);
            if (bin == null) {
                if (bins.compareAndSet(binIndex, null, newBin)) {
                    size.incrementAndGet();
                    return;
                }
            } else {
                while (true) {
                    if (bin.word.equals(word)) {
                        return;
                    } else if (bin.next == null) {
                        if (bin.compareAndSet(newBin)) {
                            size.incrementAndGet();
                            return;
                        }
                    } else {
                        bin = bin.next;
                    }
                }
            }
        }
    }

    public boolean contains(String word) {
        int binIndex = getBinIndex(word);
        for (Node n = bins.get(binIndex); n != null; n = n.next) {
            if (n.word.equals(word)) {
                return true;
            }
        }
        return false;
    }

    public int size() {
        while (true) {
            int currentSize = size.get();
            if (size.compareAndSet(currentSize, 0)) {
                return currentSize;
            }
        }
    }

    private int calculateSize() {
        //calculate size by walking through the set
        int counter = 0;
        for (int i = 0; i < bins.length(); i++) {
            Node currentBean = bins.get(i);
            if (currentBean == null) {
                continue;
            }
            counter++;
            while (currentBean.next != null) {
                currentBean = currentBean.next;
                counter++;
            }
        }
        return counter;
    }

    private int getBinIndex(String word) {
        return word.hashCode() & mask;
    }

    private static class Node {

        private static final AtomicReferenceFieldUpdater<Node, Node> nextUpdater = AtomicReferenceFieldUpdater.newUpdater(Node.class, Node.class, "next");
        private final String word;
        private volatile Node next;

        public Node(String word, Node next) {
            this.word = word;
            this.next = next;
        }

        boolean compareAndSet(Node update) {
            return nextUpdater.compareAndSet(this, null, update);
        }
    }
}
