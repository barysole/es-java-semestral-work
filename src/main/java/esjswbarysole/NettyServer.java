package esjswbarysole;

import esjswbarysole.channels.StringCounterChannelInitializer;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.net.InetSocketAddress;

public class NettyServer {

    private final int serverPort;

    public NettyServer(int portNumber) {
        this.serverPort = portNumber;
    }

    public void start() {
        EventLoopGroup eventLoopGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            serverBootstrap.group(eventLoopGroup)
                    .channel(NioServerSocketChannel.class)
                    .localAddress(new InetSocketAddress(serverPort))
                    .childHandler(new StringCounterChannelInitializer(new NonblockStringSet(1000000)));
            serverBootstrap.bind().sync();
        } catch (InterruptedException e) {
            System.out.println("Following error has been occurred during the server starting: ");
            e.printStackTrace();
        }
    }
}
