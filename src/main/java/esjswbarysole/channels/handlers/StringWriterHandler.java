package esjswbarysole.channels.handlers;

import esjswbarysole.NonblockStringSet;
import esjswbarysole.proto.Request;
import esjswbarysole.proto.Response;
import esjswbarysole.utils.GzipDecoder;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.nio.charset.StandardCharsets;
import java.util.StringTokenizer;

public class StringWriterHandler extends ChannelInboundHandlerAdapter {

    private final NonblockStringSet nonblockStringSet;

    public StringWriterHandler(NonblockStringSet nonblockStringSet) {
        super();
        this.nonblockStringSet = nonblockStringSet;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        Request request = (Request) msg;
        if (request.getMsgCase().getNumber() == Request.POSTWORDS_FIELD_NUMBER) {
            StringTokenizer inputString = new StringTokenizer(new String(GzipDecoder.decode(request.getPostWords().getData().toByteArray()), StandardCharsets.UTF_8));
            while (inputString.hasMoreTokens()) {
                nonblockStringSet.add(inputString.nextToken());
            }
            Response response = Response.newBuilder().setStatus(Response.Status.OK).build();
            ctx.writeAndFlush(response)
                    .addListener(ChannelFutureListener.CLOSE_ON_FAILURE);
        } else {
            ctx.fireChannelRead(msg);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }
}
