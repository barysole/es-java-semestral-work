package esjswbarysole.channels.handlers;

import esjswbarysole.NonblockStringSet;
import esjswbarysole.proto.Request;
import esjswbarysole.proto.Response;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class StringCountingHandler extends ChannelInboundHandlerAdapter {

    private final NonblockStringSet nonblockStringSet;

    public StringCountingHandler(NonblockStringSet nonblockStringSet) {
        super();
        this.nonblockStringSet = nonblockStringSet;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        Request request = (Request) msg;
        if (request.getMsgCase().getNumber() == Request.GETCOUNT_FIELD_NUMBER) {
            Response response = Response.newBuilder().setStatus(Response.Status.OK).setCounter(nonblockStringSet.size()).build();
            nonblockStringSet.resetSet();
            ctx.writeAndFlush(response)
                    .addListener(ChannelFutureListener.CLOSE_ON_FAILURE);
        } else {
            ctx.fireChannelRead(msg);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }
}
