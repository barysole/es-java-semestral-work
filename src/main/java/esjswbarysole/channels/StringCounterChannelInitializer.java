package esjswbarysole.channels;

import esjswbarysole.NonblockStringSet;
import esjswbarysole.channels.codecs.RequestDecoder;
import esjswbarysole.channels.codecs.ResponseEncoder;
import esjswbarysole.channels.handlers.StringCountingHandler;
import esjswbarysole.channels.handlers.StringWriterHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

public class StringCounterChannelInitializer extends ChannelInitializer<SocketChannel> {

    private final NonblockStringSet nonblockStringSet;

    public StringCounterChannelInitializer(NonblockStringSet nonblockStringSet) {
        super();
        this.nonblockStringSet = nonblockStringSet;
    }

    @Override
    protected void initChannel(SocketChannel socketChannel) {
        socketChannel.pipeline().addLast(
                new LengthFieldBasedFrameDecoder(1048576, 0, 4, 0, 4),
                new RequestDecoder(),
                new ResponseEncoder(),
                new StringWriterHandler(nonblockStringSet),
                new StringCountingHandler(nonblockStringSet));
    }
}
