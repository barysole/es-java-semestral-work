package esjswbarysole.channels.codecs;

import esjswbarysole.proto.Response;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class ResponseEncoder extends MessageToByteEncoder<Response> {

    @Override
    protected void encode(ChannelHandlerContext ctx, Response response, ByteBuf out) {
        int messageSize = response.getSerializedSize();
        out.writeInt(messageSize);
        out.writeBytes(response.toByteArray());
    }

}
